create table vp_user
(
    id bigserial primary key,
    name     text not null,
    email    text not null,
    password text not null,
    constraint u_user_email unique (email)
);
create index user_id_index on vp_user (id);
create index user_name_index on vp_user (name, email);

create table project
(
    id bigserial primary key,
    title       text      not null,
    type        text      not null,
    created_at  timestamp not null,
    modified_at timestamp not null
);

create table user_project
(
    id bigserial primary key,
    vp_user_id    bigint not null,
    project_id bigint not null,
    is_favorite boolean not null default false,
    constraint u_user_project_id unique (project_id, vp_user_id),
    constraint fk_ut_project_id foreign key (project_id) references project (id),
    constraint fk_ut_user_id foreign key (vp_user_id) references vp_user (id)
);
create unique index user_project_to_user_index on user_project (vp_user_id, project_id);
