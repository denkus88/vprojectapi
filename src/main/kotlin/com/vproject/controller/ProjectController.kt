package com.vproject.controller

import com.vproject.model.ProjectDto
import com.vproject.service.ProjectService
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.time.LocalDateTime

@RestController
@RequestMapping("projects")
class ProjectController(
    val projectService: ProjectService
) {

    @GetMapping
    fun getAllProjects() = projectService.getAllProjects()

    @DeleteMapping("/{id}")
    fun deleteProject(@PathVariable("id") id: Long) = let {
        projectService.deleteById(id)
    }

    @PostMapping
    @Throws(ResponseStatusException::class)
    fun createProject(@RequestBody projectDto: ProjectDto) = projectService.createProject(projectDto)

    @PutMapping
    fun updateProject(@RequestBody projectDto: ProjectDto) = projectService.updateProject(projectDto)

    @GetMapping("/search")
    fun findProjectsByEmail(
        @RequestParam("email") email: String?,
        @RequestParam("isFavorite") isFavorite: Boolean?
    ) = projectService.findProjectsByParams(email, isFavorite)
}
