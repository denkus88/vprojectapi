package com.vproject.controller

import com.vproject.model.VpUser
import com.vproject.model.VpUserDto
import com.vproject.model.dto.ErrorDto
import com.vproject.service.UserService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus.*
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("users")
class UserController(
    val userService: UserService
) {

    @GetMapping
    @ResponseStatus(OK)
    @Operation(
        summary = "Return a list of users ",
        description = "Return a list of users",
        tags = ["users"]
    )
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "OK",
                content = [Content(schema = Schema(implementation = VpUserDto::class))]
            ),
            ApiResponse(
                responseCode = "400", description = "Bad request",
                content = [Content(schema = Schema(implementation = ErrorDto::class))]
            )
        ]
    )
    fun getAllUser() = userService.getAllUser()

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    @Operation(
        summary = "Return user by id",
        description = "Return user by id",
        tags = ["user"]
    )
    fun getUserById(@PathVariable("id") id: Long) = userService.getUserById(id)

    @PostMapping
    @ResponseStatus(CREATED)
    @Operation(summary = "Create user", description = "Create user", tags = ["user"])
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "201", description = "successful operation",
                content = [Content(schema = Schema(implementation = VpUserDto::class))]
            ),
            ApiResponse(
                responseCode = "400", description = "Bad request",
                content = [Content(schema = Schema(implementation = ErrorDto::class))]
            )
        ]
    )
    @Throws(ResponseStatusException::class)
    fun createUser(@RequestBody vpUserDto: VpUserDto) = userService.createUser(vpUserDto)

    @PutMapping
    fun updateUser(@RequestBody vpUserDto: VpUserDto) = userService.updateUser(vpUserDto)

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    fun deleteUser(@PathVariable("id") id: Long) = userService.deleteUserById(id)

}