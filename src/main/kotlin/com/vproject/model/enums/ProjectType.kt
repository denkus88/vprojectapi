package com.vproject.model.enums

enum class ProjectType {
    NORMAL,
    URGENT,
    SUSPENDED
}