package com.vproject.model.dto

data class ErrorDto(
    val code: Int,
    val description: String
)