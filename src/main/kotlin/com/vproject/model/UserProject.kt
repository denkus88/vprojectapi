package com.vproject.model

import javax.persistence.*

@Entity
data class UserProject(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @ManyToOne
    @JoinColumn(name = "vp_user_id")
    val vpUser: VpUser,

    @ManyToOne
    @JoinColumn(name = "project_id")
    val project: Project,

    @Column
    val isFavorite: Boolean
)