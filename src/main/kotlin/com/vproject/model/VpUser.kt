package com.vproject.model

import io.swagger.v3.oas.annotations.media.Schema
import javax.persistence.*
import javax.persistence.CascadeType.ALL
import javax.persistence.FetchType.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank

@Entity
data class VpUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column
    @Email
    val email: String,

    @Column
    @NotBlank(message = "Name is mandatory")
    val name: String,

    @Column
    @NotBlank(message = "Password is mandatory")
    val password: String,

    @OneToMany(cascade = [ALL], mappedBy = "vpUser")
    val userProject: MutableList<UserProject> = mutableListOf()
)

data class VpUserDto(
    @Schema(description = "Unique identifier of the user.", example = "1")
    val id: Long = 0,

    @Schema(description = "Email", example = "carl@google.com", required = true)
    val email: String,

    @Schema(description = "User Name", example = "Carl", required = true)
    val name: String,

    @Schema(description = "Password", required = true)
    val password: String,

    @Schema(description = "User Projects")
    val projects: MutableList<ProjectFavoriteDto>? = mutableListOf(),

    @Schema(description = "Favorite Projects")
    val favoriteProjects: MutableList<ProjectFavoriteDto>? = mutableListOf()
)


data class ProjectFavoriteDto(
    val name: String,
    val id: Long
)
