package com.vproject.model

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.vproject.model.enums.ProjectType
import com.vproject.model.enums.ProjectType.NORMAL
import org.springframework.format.annotation.DateTimeFormat
import java.sql.Timestamp
import java.time.LocalDateTime
import javax.persistence.*
import javax.persistence.CascadeType.ALL
import javax.persistence.EnumType.STRING
import javax.persistence.GenerationType.IDENTITY

@Entity
data class Project(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = 0,

    @Column
    val title: String,

    @Column
    @Enumerated(STRING)
    val type: ProjectType = NORMAL,

    @Column(name = "created_at" , updatable = false)
    @DateTimeFormat(style = "yyyy-mm-ddThh:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val created: LocalDateTime = LocalDateTime.now(),

    @Column(name = "modified_at")
    @DateTimeFormat(style = "yyyy-mm-ddThh:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val modified: LocalDateTime,

    @JsonIgnore
    @OneToMany(cascade = [ALL], mappedBy = "project")
    val userProject: List<UserProject> = listOf()

)

data class ProjectDto(
    val id: Long,
    val title: String,
    val type: ProjectType
)

interface ProjectsForUser{
    val id: Long
    val title: String
    val type: ProjectType
    val created: LocalDateTime
    val modified: LocalDateTime
}

data class  ProjectsUserDto(
    override val id: Long,
    override val title: String,
    override val type: ProjectType,
    override val created: LocalDateTime,
    override val modified: LocalDateTime
) : ProjectsForUser

