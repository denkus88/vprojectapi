package com.vproject.repository

import com.vproject.model.Project
import com.vproject.model.ProjectsForUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository : JpaRepository<Project, Long> {

    @Query(
        value = """
    
SELECT pr.id          AS id,
       pr.title       AS title,
       pr.type        AS type,
       pr.created_at  AS created,
       pr.modified_at AS modified
FROM project pr
         JOIN user_project up on pr.id = up.project_id
         JOIN vp_user vu on up.vp_user_id = vu.id
WHERE (:email IS NULL OR vu.email = :email)
    """, nativeQuery = true
    )
    fun findUserProjectUsingParams(
        @Param(value = "email") email: String?
    ): List<ProjectsForUser>

    @Query(
        value = """
        select *
        FROM project p 
        WHERE p.id IN (:ids)
        """, nativeQuery = true
    )
    fun getProjectsByIds(@Param("ids") ids: List<Long>): List<Project>
}