package com.vproject.repository

import com.vproject.model.UserProject
import com.vproject.model.VpUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserProjectRepository : JpaRepository<UserProject, Long> {

    fun deleteByVpUser(vpUser: VpUser)

    fun findUserProjectsByVpUserId(id: Long):List<UserProject>
}