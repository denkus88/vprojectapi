package com.vproject.repository

import com.vproject.model.VpUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<VpUser, Long> {

    @Query(
        value = """
          select *
          from vp_user vp
          where vp.email = :email
    """, nativeQuery = true
    )
    fun findUserByEmail(email: String): VpUser?

    fun findVpUserById(id: Long): VpUser
}