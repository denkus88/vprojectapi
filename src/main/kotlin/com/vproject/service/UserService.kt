package com.vproject.service

import com.vproject.model.ProjectFavoriteDto
import com.vproject.model.UserProject
import com.vproject.model.VpUser
import com.vproject.model.VpUserDto
import com.vproject.repository.ProjectRepository
import com.vproject.repository.UserProjectRepository
import com.vproject.repository.UserRepository
import com.vproject.utils.getLogger
import org.apache.commons.validator.routines.EmailValidator
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import javax.transaction.Transactional


@Service
class UserService(
    val userRepository: UserRepository,
    val projectRepository: ProjectRepository,
    val userProjectRepository: UserProjectRepository,
    val passwordEncoder: PasswordEncoder
) {
    val logger = getLogger()

    fun getAllUser(): List<VpUserDto> = userRepository.findAll().map { vpUser ->
        vpUser.mapToVpUserDto()
    }

    fun getUserById(id: Long) = userRepository.findById(id).orElseThrow()?.mapToVpUserDto()

    fun deleteUserById(id: Long) = userRepository.deleteById(id)

    @Transactional
    fun createUser(vpUserDto: VpUserDto) {
        if (userRepository.findUserByEmail(vpUserDto.email)?.email.isNullOrEmpty()) {
            logger.info("Create user for email: ${vpUserDto.email}")
            if (vpUserDto.email.isValidEmailAddress() && vpUserDto.name.isNotEmpty() && vpUserDto.password.isNotEmpty()) {
                val vpUser = VpUser(
                    id = vpUserDto.id,
                    email = vpUserDto.email,
                    name = vpUserDto.name,
                    password = passwordEncoder.encode(vpUserDto.password),
                    userProject = mutableListOf()
                )
                val savedUser = userRepository.save(vpUser)
                if (!vpUserDto.projects.isNullOrEmpty()) getUserProjects(vpUserDto.projects, savedUser, vpUserDto)
            } else {
                throw ResponseStatusException(BAD_REQUEST, "Invalid data please try again.")
            }
        } else {
            throw ResponseStatusException(BAD_REQUEST, "Email already exist")
        }
    }

    private fun getUserProjects(
        projects: MutableList<ProjectFavoriteDto>,
        vpUser: VpUser,
        vpUserDto: VpUserDto
    ) = projects.let { ids -> projectRepository.getProjectsByIds(ids.map { it.id }) }
        .map { project ->
            UserProject(
                vpUser = vpUser,
                project = project,
                isFavorite = vpUserDto.favoriteProjects?.any { it.id == project.id } ?: false
            )
        }.let { userProjectRepository.saveAll(it) }.toMutableList()

    @Transactional
    fun updateUser(vpUserDto: VpUserDto) = userRepository.findUserByEmail(vpUserDto.email)
        .let { vpUserDtos ->
            logger.info("Update user with email:${vpUserDto.email}")
            val vpUser = VpUser(
                id = vpUserDtos!!.id,
                email = vpUserDto.email,
                name = vpUserDto.name,
                password = if (vpUserDto.password.isEmpty()) userRepository.findVpUserById(vpUserDtos.id).password else passwordEncoder.encode(
                    vpUserDto.password
                ),
                userProject = mutableListOf()
            )
            val res = getUserProjectData(vpUserDto, vpUser)
            if (res.isEmpty()) {
                userRepository.save(vpUser).also { userProjectRepository.deleteByVpUser(it) }
            }
        }

    private fun getUserProjectData(vpUserDto: VpUserDto, vpUser: VpUser) =
        if (!vpUserDto.projects.isNullOrEmpty()) {
            val ex = userProjectRepository.findUserProjectsByVpUserId(vpUser.id)
            if (!ex.isNullOrEmpty()) userProjectRepository.deleteByVpUser(vpUser)
            vpUserDto.projects.let { ids -> projectRepository.getProjectsByIds(ids.map { it.id }) }
                .map { project ->
                    UserProject(
                        vpUser = vpUser,
                        project = project,
                        isFavorite = vpUserDto.favoriteProjects?.any { it.id == project.id } ?: false
                    )
                }.let { userProjectRepository.saveAll(it) }
                .toMutableList()
        } else mutableListOf()

    private fun VpUser.mapToVpUserDto() =
        VpUserDto(
            id = id,
            email = email,
            password = password,
            name = name,
            projects = userProject.map { ProjectFavoriteDto(it.project.title, it.project.id) }.toMutableList(),
            favoriteProjects = userProject.filter { it.isFavorite }.map {
                ProjectFavoriteDto(
                    it.project.title,
                    it.id
                )
            }.toMutableList()
        )

    private fun String.isValidEmailAddress() = EmailValidator.getInstance().isValid(this);
}