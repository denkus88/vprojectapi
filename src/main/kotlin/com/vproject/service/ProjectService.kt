package com.vproject.service

import com.vproject.model.Project
import com.vproject.model.ProjectDto
import com.vproject.model.ProjectsUserDto
import com.vproject.repository.ProjectRepository
import com.vproject.utils.getLogger
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.sql.Timestamp
import java.time.LocalDateTime
import javax.transaction.Transactional

@Service
class ProjectService(
    val projectRepository: ProjectRepository
) {
    val logger = getLogger()

    fun getAllProjects(): List<ProjectsUserDto> = projectRepository.findAll().map { projectDto ->
        ProjectsUserDto(
            id = projectDto.id,
            title = projectDto.title,
            type = projectDto.type,
            created = projectDto.created,
            modified = projectDto.modified
        )
    }

    fun deleteById(id: Long) = projectRepository.deleteById(id)

    @Transactional
    fun createProject(projectDto: ProjectDto) =
        if (projectDto.title.isNotEmpty()) {
            projectRepository.saveAndFlush(
                Project(
                    title = projectDto.title,
                    type = projectDto.type,
                    created = LocalDateTime.now(),
                    modified = LocalDateTime.now(),
                    userProject = mutableListOf()
                )
            ).also { logger.info("Project with title ${projectDto.title} and type ${projectDto.type} was created") }
        } else throw ResponseStatusException(BAD_REQUEST, "Title should not be empty")

    @Transactional
    fun updateProject(projectDto: ProjectDto) =
        projectRepository.findById(projectDto.id).get().let { project ->
            projectRepository.saveAndFlush(
                project.copy(
                    id = project.id,
                    title = projectDto.title,
                    type = projectDto.type,
                    created = project.created,
                    modified = LocalDateTime.now(),
                    userProject = mutableListOf()
                )
            )
        }.also {
            logger.info("Project with title ${projectDto.title} and type ${projectDto.type} was updated")
        }

    fun findProjectsByParams(email: String?, isFavorite: Boolean?) =
        projectRepository.findUserProjectUsingParams(email).map { projectForUserData ->
            Project(
                id = projectForUserData.id,
                title = projectForUserData.title,
                type = projectForUserData.type,
                created = projectForUserData.created,
                modified = projectForUserData.modified,
                userProject = mutableListOf()
            )
        }

}