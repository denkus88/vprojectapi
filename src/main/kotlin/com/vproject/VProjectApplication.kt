package com.vproject

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VProjectApplication

fun main(args: Array<String>) {
    runApplication<VProjectApplication>(*args) {
        setBannerMode(Banner.Mode.OFF)
    }
}
