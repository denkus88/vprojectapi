package com.vproject.service

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vproject.model.VpUser
import com.vproject.model.VpUserDto
import com.vproject.repository.ProjectRepository
import com.vproject.repository.UserProjectRepository
import com.vproject.repository.UserRepository
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.`when`
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.server.ResponseStatusException

class UserServiceTest {
    private val userRepositoryMock = mock<UserRepository>()
    private val projectRepositoryMock = mock<ProjectRepository>()
    private val passwordEncoderMock = mock<PasswordEncoder>()
    private val userProjectRepositoryMock = mock<UserProjectRepository>()
    private val userService = UserService(
        userRepository = userRepositoryMock,
        projectRepository = projectRepositoryMock,
        userProjectRepository = userProjectRepositoryMock,
        passwordEncoder = passwordEncoderMock
    )

    @Test
    fun `should create new user`() {

        `when`(userRepositoryMock.findUserByEmail(anyString())).thenReturn(user.copy(email = ""))
        `when`(userRepositoryMock.save(savedUser)).thenReturn(savedUser)
        `when`(passwordEncoderMock.encode(anyString())).thenReturn(savedUser.password)

        userService.createUser(vpUser)

        verify(userRepositoryMock, times(1)).save(savedUser)
    }

    @Test(expected = ResponseStatusException::class)
    fun `should  throw exception if email already exists`() {

        val vpUserWithEmail = vpUser
        `when`(userRepositoryMock.findUserByEmail(anyString())).thenReturn(user)

        userService.createUser(vpUserWithEmail)
    }


    companion object {
        private const val ALICE_EMAIL = "Alice@gmail.com"
        private val user = VpUser(
            id = 0L,
            email = ALICE_EMAIL,
            name = "Alice",
            password = "23456"
        )
        private val vpUser = VpUserDto(
            id = 0L,
            email = ALICE_EMAIL,
            name = "Ivan",
            password = "3333",
            projects = mutableListOf(),
            favoriteProjects = mutableListOf()
        )
        private val savedUser = VpUser(
            id = 0L,
            email = ALICE_EMAIL,
            name = "Ivan",
            password = "3333",
            userProject = mutableListOf()
        )
    }
}