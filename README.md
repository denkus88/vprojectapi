# Access Management Tool Project

**Steps:**

**DB settings:**

1. DB Docker container:
`docker run -d --name pg12 -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -v pg12_data_volume:/var/lib/postgresql/data postgres:12.2`;

2. Run postgres db:
`docker exec -it pg12 psql -U postgres`;

3. Create user and password:
`create role vproject with login encrypted  password 'vproject'`;

4. Set owner:
`create database vproject owner vproject encoding 'UTF-8';`

**Start application:**

5. Run application
`./gradlew clean bootRun`



